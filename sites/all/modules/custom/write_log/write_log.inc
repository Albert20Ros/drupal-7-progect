<?php

function write_log_message_form($form, &$form_state) {
  $form['message'] = array(
    '#title' => t('Message'),
    '#type' => 'textarea',
    '#required' => TRUE,
  );

  if (user_is_anonymous()) {
    $form['name'] = array(
      '#title' => t('Full name'),
      '#type' => 'textfield',
    );


    if (variable_get('write_log_additional_information') == 1) {

      $form['phone'] = array(
        '#title' => t('Phone'),
        '#type' => 'textfield',
        '#description' => t('Example phone number +79261234567, 89261234567, 8(926)123-45-67'),
      );
      $form['email'] = array(
        '#title' => t('Email'),
        '#type' => 'textfield',
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Send',
  );

  return $form;

}


function write_log_settings_form($form, &$form_state) {
  $form['additional_information'] = array(
    '#title' => t('Additional information'),
    '#type' => 'checkbox',
    '#default_value' => !empty(variable_get('write_log_additional_information')) ? variable_get('write_log_additional_information') : 0,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );

  return $form;
}


function write_log_settings_form_submit($form, &$form_state) {
  variable_set('write_log_additional_information', $form_state['values']['additional_information']);
}

/**
 * Implements hook_form().
 */
function write_log_message_form_validate($form, &$form_state) {
  if (isset($form_state['values']['phone'])) {

    $number = $form_state['values']['phone'];

    if (!preg_match('^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$^', $number)) {
      form_set_error('phone', t('Incorrect form number.'));
    }
  }

  if (isset($form_state['values']['email'])) {

    $email = $form_state['values']['email'];

    if (!valid_email_address($email)) {
      form_set_error('email', t('Incorrect form email'));
    }
  }


}

function write_log_message_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  drupal_set_message(t('Thanks for form!!!)'));
  if (isset($values['name'])) {
    watchdog('write_log','Message: message = %message , name = %name',array('%message' => $values['message'], '%name' => $values['name'] ));
  }
  else {
    global $user;
    watchdog('write_log','Message: message = %message , name = %name',array('%message' => $values['message'], '%name' => $user->name ));
  }
}
