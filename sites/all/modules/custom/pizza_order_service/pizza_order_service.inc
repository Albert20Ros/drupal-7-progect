<?php


function pizza_order_service($form, &$form_state) {


  $query_pizza = db_select('pizza_order_service_pizza')
    ->fields('pizza_order_service_pizza', array(
      'name',
      'price',
      'count',
      'available'
    ));

  $result_pizza = $query_pizza->execute();

  $form['pizza'] = [
    '#type' => 'fieldset',
    '#title' => t('Pizza'),
    '#tree' => TRUE,
  ];
$example = "Pizza page title - pizza order service"

  while ($pizza = $result_pizza->fetchAssoc()) {

    if ($pizza['available'] == 1) {

      $form['pizza'][$pizza['name']] = [
        '#type' => 'fieldset',
        '#title' => t($pizza['name'] . ' Price: ' . $pizza['price'] . ' ₽'),
        '#description' => t('Name pizza.'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => TRUE,
      ];
      $form['pizza'][$pizza['name']]['count'] = array(
        '#type' => 'select',
        '#title' => t('Count  pizza'),
        '#options' => range(0, $pizza['count']),
        '#id' => $pizza['price'],
      );
    }
  }


  $query_blocks = db_select('pizza_order_service_blocks')
    ->fields('pizza_order_service_blocks', array('name', 'price'));


  $result_blocks = $query_blocks->execute();

  $blocks_array = array();

  while ($blocks = $result_blocks->fetchAssoc()) {
    $blocks_array[$blocks['price']] = $blocks['name'];
  }

  $form['district'] = [
    '#title' => t('district'),
    '#type' => 'radios',
    '#options' => $blocks_array,
    '#required' => TRUE
  ];


  $form['phone'] = [
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#required' => TRUE
  ];


  $form['address'] = [
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#required' => TRUE
  ];


  $form['price-order'] = [
    '#type' => 'textfield',
    '#title' => t('Price'),
    '#disabled' => TRUE,
    '#id' => 'price-order',
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('place an order'),
  ];


  $form['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'pizza_order_service') . '/pizza_order_service.js',
    'type' => 'file',
  );
  drupal_add_js(drupal_get_path('module', 'pizza_order_service') . '/example.js');
  drupal_add_js(array('example' => $example,), 'setting');
  return $form;
}

function pizza_order_service_validate($form, &$form_state) {

  if (isset($form_state['values']['phone'])) {
    $phone = $form_state['values']['phone'];

    if (!preg_match('^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$^', $phone)) {
      form_set_error('phone', t('Phone write ERROR'));
    }
  }


}

function pizza_order_service_submit($form, &$form_state) {
  pizza_order_service_add_order_in_bd($form_state);

  $order_price = 0;

  $count_order = ' ';


  foreach ($form_state['values']['pizza'] as $key => $pizza) {

    $count_order .= $key. ' = ' . $pizza['count']. ', ';

    $price = db_select('pizza_order_service_pizza')
      ->fields('pizza_order_service_pizza', array('price'))
      ->condition('name', $key, '=')
      ->execute()
      ->fetchField();


    $order_price += $pizza['count'] * $price;
  }

  $order_price += $form_state['values']['district'];



  $module = 'pizza_order_service';
  $key = 'pizza_order_service';
  $to = variable_get('site_mail', '');
  $language = 'ru';

  $params = array(
    'order_price' => $order_price,
    'count_order' => $count_order,
    'phone' => $form_state['values']['phone'],
    'block' => $form['district']['#options'][$form_state['values']['district']],
    'address' => $form_state['values']['address'],
  );

  drupal_mail($module, $key, $to, $language, $params, $from = NULL, $send = TRUE);


  drupal_set_message("Спасибо, ваша пицца скоро будет доставленна.");

}

function pizza_order_service_settings($form, &$form_state) {

  $form['title_one'] = [
    '#markup' => 'pizza order service settings',
  ];

  $query_pizza = db_select('pizza_order_service_pizza')
    ->fields('pizza_order_service_pizza', array(
      'name',
      'price',
      'count',
      'available'
    ));

  $result_pizza = $query_pizza->execute();
  while ($pizza = $result_pizza->fetchAssoc()) {

    if ($pizza['available'] == 1) {
      $available = TRUE;
    }
    else {
      $available = FALSE;
    }
    $form['pizza_' . $pizza['name']] = [
      '#type' => 'fieldset',
      '#title' => t($pizza['name']),
      '#description' => t('Name pizza.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    ];
    $form['pizza_' . $pizza['name']]['count'] = [
      '#markup' => $pizza['count'],
    ];
    $form['pizza_' . $pizza['name']]['count_change'] = [
      '#type' => 'textfield',
      '#title' => t('Change count of pizza'),
    ];

    $form['pizza_' . $pizza['name']]['price'] = [
      '#markup' => $pizza['price'] . ' ₽',
    ];


    $form['pizza_' . $pizza['name']]['price_change'] = [
      '#type' => 'textfield',
      '#title' => t('Change price of pizza order'),
    ];

    if ($pizza['count'] == 0) {
      $disabled = TRUE;
    }
    else {
      $disabled = FALSE;
    }

    $form['pizza_' . $pizza['name']]['available'] = [
      '#type' => 'checkbox',
      '#title' => t('Available'),
      '#default_value' => $available,
      '#disabled' => $disabled,
    ];
  }

  $form['title_two'] = [
    '#markup' => 'district order config',
  ];

  $query_blocks = db_select('pizza_order_service_blocks')
    ->fields('pizza_order_service_blocks', array(
      'name',
      'price',
    ));


  $result_blocks = $query_blocks->execute();
  while ($blocks = $result_blocks->fetchAssoc()) {
    $form['block_' . $blocks['name']] = [
      '#type' => 'fieldset',
      '#title' => t($blocks['name']),
      '#description' => t('Name Block.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    ];

    $form['block_' . $blocks['name']]['price'] = [
      '#markup' => $blocks['price'] . ' ₽',
    ];


    $form['block_' . $blocks['name']]['price_change'] = [
      '#type' => 'textfield',
      '#title' => t('Change price of pizza'),
    ];
  }

  $form['submit'] = [
    "#type" => "submit",
    '#value' => t('Change config pizza order'),
  ];


  return $form;

}

function pizza_order_service_settings_submit($form, &$form_state) {
  foreach ($form_state['complete form'] as $key => $pizza) {

    if (strpos($key, 'pizza') !== FALSE) {

      if ($form_state['values'][$key]['count_change']) {
        pizza_order_service_change_pizza_bd($key, $form_state['values'][$key]['count_change'], 'pizza_order_service_pizza', 'count', 'pizza_');
      }

      if ($form_state['values'][$key]['price_change']) {
        pizza_order_service_change_pizza_bd($key, $form_state['values'][$key]['price_change'], 'pizza_order_service_pizza', 'price', 'pizza_');
      }

      if ($form_state['values'][$key]['available'] !== FALSE) {
        pizza_order_service_change_pizza_bd($key, $form_state['values'][$key]['available'], 'pizza_order_service_pizza', 'available', 'pizza_');
      }

    }
  }

  foreach ($form_state['complete form'] as $key => $blocks) {


    if (strpos($key, 'block') !== FALSE) {
      drupal_set_message($key);
      if ($form_state['values'][$key]['price_change']) {
        pizza_order_service_change_pizza_bd($key, $form_state['values'][$key]['price_change'], 'pizza_order_service_blocks', 'price', 'block_');
      }
    }
  }

}
function pizza_order_service_change_pizza_bd($name, $value, $table, $field, $clear_str) {
  $name = str_replace($clear_str, '', $name);
  db_update($table)
    ->condition('name', $name)
    ->fields(array(
      $field => $value,
    ))
    ->execute();
}
function pizza_order_service_add_order_in_bd($form_state) {


  foreach ($form_state['values']['pizza'] as $key => $pizza) {


    if ($pizza['count'] !== 0) {

      $count = db_select('pizza_order_service_pizza')
        ->fields('pizza_order_service_pizza', array('count'))
        ->condition('name', $key, '=')
        ->execute()
        ->fetchField();
      $count -= $pizza['count'];

      db_update('pizza_order_service_pizza')
        ->condition('name', $key)
        ->fields(array(
          'count' => $count,
        ))
        ->execute();

      if ($count == 0) {
        db_update('pizza_order_service_pizza')
          ->condition('name', $key)
          ->fields(array(
            'available' => 0,
          ))
          ->execute();
      }

    }

  }


}

